package ru.t1.dkozoriz.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.comparator.CreatedComparator;
import ru.t1.dkozoriz.tm.comparator.NameComparator;
import ru.t1.dkozoriz.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator<? super IWBS> comparator;

    @Nullable
    public static Sort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    Sort(@NotNull final String displayName, @NotNull final Comparator<? super IWBS> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator<? super IWBS> getComparator() {
        return comparator;
    }

}