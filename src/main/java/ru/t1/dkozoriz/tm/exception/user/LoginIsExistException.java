package ru.t1.dkozoriz.tm.exception.user;

import ru.t1.dkozoriz.tm.exception.field.AbstractFieldException;

public final class LoginIsExistException extends AbstractFieldException {

    public LoginIsExistException() {
        super("Error! Login is exist.");
    }

}