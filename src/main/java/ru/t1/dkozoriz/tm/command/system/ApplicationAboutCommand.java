package ru.t1.dkozoriz.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public ApplicationAboutCommand() {
        super("about", "show developer info.", "-a");
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getServiceLocator().getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getServiceLocator().getPropertyService().getAuthorEmail());
    }

}